//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqTruchasProjectLoader_h
#define pqTruchasProjectLoader_h

#include "smtk/PublicPointerDefs.h"

#include <QObject>

class pqServer;
class pqServerResource;

/** \brief Handles opening projects from main menu and recent projects list
  */
class pqTruchasProjectLoader : public QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  static pqTruchasProjectLoader* instance(QObject* parent = nullptr);
  ~pqTruchasProjectLoader() override;

  // Indicates if paraview resource can be loaded
  bool canLoad(const pqServerResource& resource) const;

  // Open project from path on server
  bool load(pqServer* server, const QString& path);

  // Open project from recent projects list
  bool load(const pqServerResource& resource);

signals:
  void projectOpened(smtk::project::ProjectPtr);

protected:
  pqTruchasProjectLoader(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqTruchasProjectLoader);
};

#endif // pqTruchasProjectLoader_h
