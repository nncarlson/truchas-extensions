//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqTruchasProjectCloseBehavior.h"

#include "smtk/simulation/truchas/plugin/pqTruchasProjectSaveBehavior.h"
#include "smtk/simulation/truchas/qt/qtProjectRuntime.h"

// SMTK
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResource.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"

// // Client side
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqObjectBuilder.h"
#include "pqServer.h"

#include <QAction>
#include <QDebug>
#include <QMessageBox>
#include <QtGlobal>

#include <vector>

//-----------------------------------------------------------------------------
pqTruchasProjectCloseReaction::pqTruchasProjectCloseReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqTruchasProjectCloseReaction::closeProject()
{
  // Get current project
  auto project = qtProjectRuntime::instance()->project();
  if (project == nullptr)
  {
    qWarning() << "Internal error - no active project.";
    return;
  }

  // Check if project is modified
  if (!project->clean())
  {
    QMessageBox msgBox;
    msgBox.setText("The project has been modified.");
    msgBox.setInformativeText("Do you want to save your changes?");
    auto buttons = QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel;
    msgBox.setStandardButtons(buttons);
    msgBox.setDefaultButton(QMessageBox::Save);

    int ret = msgBox.exec();
    if (ret == QMessageBox::Cancel)
    {
      return;
    }
    else if (ret == QMessageBox::Save)
    {
      if (!pqTruchasProjectSaveBehavior::instance()->saveProject())
      {
        return;
      }
    }
  } // if (project modified)

  // Access the active server and get the project manager
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto projectManager = wrapper->smtkProjectManager();
  auto projectName = project->name();
  auto resManager = std::static_pointer_cast<smtk::resource::Resource>(project)->manager();

  // Mark all resources for removal first
  for (auto iter = project->resources().begin(); iter != project->resources().end(); ++iter)
  {
    auto resource = *iter;
    resource->setMarkedForRemoval(true);
  }

  // Now remove resources from resource manager
  for (auto iter = project->resources().begin(); iter != project->resources().end(); ++iter)
  {
    resManager->remove(*iter);
  }

  // Update the runtime instance
  qtProjectRuntime::instance()->unsetProject(project);

  // Remove the project from *both* resource manager & project manager
  resManager->remove(project);
  projectManager->remove(project);

  qInfo() << "Closed project" << projectName.c_str();
  emit this->projectClosed();
} // closeProject()

//-----------------------------------------------------------------------------
static pqTruchasProjectCloseBehavior* g_instance = nullptr;

pqTruchasProjectCloseBehavior::pqTruchasProjectCloseBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqTruchasProjectCloseBehavior* pqTruchasProjectCloseBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqTruchasProjectCloseBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqTruchasProjectCloseBehavior::~pqTruchasProjectCloseBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
