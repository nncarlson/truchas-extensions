//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_truchas_plugin_pqTruchasProjectMenu_h
#define smtk_simulation_truchas_plugin_pqTruchasProjectMenu_h

#include <QActionGroup>

#include "smtk/project/Project.h"

class QAction;

class pqTruchasRecentProjectsMenu;

/** \brief Adds a "Truchas" menu to the application main window
  */
class pqTruchasProjectMenu : public QActionGroup
{
  Q_OBJECT
  using Superclass = QActionGroup;

signals:

public slots:
  void onProjectOpened(std::shared_ptr<smtk::project::Project> project);
  void onProjectClosed();

public:
  pqTruchasProjectMenu(QObject* parent = nullptr);
  ~pqTruchasProjectMenu() override;

  bool startup();
  void shutdown();

private:
  QAction* m_newProjectAction = nullptr;
  QAction* m_openProjectAction;
  QAction* m_recentProjectsAction;
  QAction* m_saveProjectAction;
  // QAction* m_saveAsProjectAction;  // not supported
  QAction* m_closeProjectAction;
  QAction* m_exportProjectAction = nullptr;
  QAction* m_importModelAction;

  pqTruchasRecentProjectsMenu* m_recentProjectsMenu;

  Q_DISABLE_COPY(pqTruchasProjectMenu);
};

#endif // pqTruchasProjectMenu_h
