//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "qtSharedPropertiesItem.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/ValueItem.h"
#include "smtk/extension/qt/qtBaseAttributeView.h"
#include "smtk/extension/qt/qtInputsItem.h"
#include "smtk/extension/qt/qtItem.h"
#include "smtk/extension/qt/qtUIManager.h"
#include "smtk/view/Configuration.h"

#include "smtk/simulation/truchas/qt/ctkCollapsibleButton.h"
#include "smtk/simulation/truchas/qt/qtMaterialAttribute.h"

#include <QDebug>
#include <QFrame>
#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QVBoxLayout>

#include <map>

#define ALERT_ICON_PATH ":/icons/attribute/errorAlert.png"

class qtSharedPropertiesItemInternals
{
public:
  qtMaterialAttribute* m_qAttribute;
  smtk::attribute::GroupItemPtr m_phasesItem;
  smtk::attribute::GroupItemPtr m_sharedItem;
  QLabel* m_alertLabel;
  QWidget* m_itemFrame;
  std::map<std::string, qtItemInfo> m_itemViewMap;
  std::map<std::string, smtk::extension::qtItem*> m_qtItemMap; // <item name, qtItem*>

  qtSharedPropertiesItemInternals()
    : m_qAttribute(nullptr)
    , m_alertLabel(nullptr)
    , m_itemFrame(nullptr)
  {
  }
};

static smtk::extension::qtItem* createItemWidget(
  const qtItemInfo& info, qtMaterialAttribute* attribute)
{
  return new qtSharedPropertiesItem(info, attribute);
}

qtSharedPropertiesItem::qtSharedPropertiesItem(
  const qtItemInfo& info, qtMaterialAttribute* qAttribute)
  : smtk::extension::qtItem(info)
{
  this->Internals = new qtSharedPropertiesItemInternals;
  this->Internals->m_qAttribute = qAttribute;
  m_itemInfo.createNewDictionary(this->Internals->m_itemViewMap);
  auto att = qAttribute->attribute();
  this->Internals->m_phasesItem = att->findGroup("phases");
  this->Internals->m_sharedItem = att->findGroup("shared-properties");

  this->createWidget();
}

qtSharedPropertiesItem::~qtSharedPropertiesItem()
{
  delete this->Internals;
}

void qtSharedPropertiesItem::updateItemData()
{
  bool makeVisible = this->Internals->m_phasesItem->numberOfGroups() > 1;
  if (makeVisible)
  {
    this->buildItemFrame();
  }
  m_widget->setVisible(makeVisible);
}

void qtSharedPropertiesItem::onItemModified()
{
  auto qItem = qobject_cast<smtk::extension::qtItem*>(this->sender());
  if (qItem == nullptr)
  {
    return;
  }
  emit this->modified();

  // Check validity
  bool isValid = this->Internals->m_sharedItem->isValid();
  this->Internals->m_alertLabel->setVisible(!isValid);
  smtk::attribute::ItemPtr item = qItem->item();
  emit this->itemEnabledStateChanged(QString::fromStdString(item->name()), item->isEnabled());
}

void qtSharedPropertiesItem::onPhaseCountChanged()
{
  bool makeVisible = this->Internals->m_phasesItem->numberOfGroups() > 1;
  m_widget->setVisible(makeVisible);
}

void qtSharedPropertiesItem::createWidget()
{
  if (this->Internals->m_sharedItem == nullptr)
  {
    return;
  }

  auto parentWidget = this->Internals->m_qAttribute->widget();
  auto* topFrame = new QFrame(parentWidget);
  auto* topLayout = new QHBoxLayout(topFrame);

  auto* label = new QLabel(topFrame);
  auto icon = QIcon(ALERT_ICON_PATH);
  label->setPixmap(icon.pixmap(16));
  label->setMinimumWidth(24);
  topLayout->addWidget(label);

  bool isValid = this->Internals->m_sharedItem->isValid();
  label->setVisible(!isValid);
  this->Internals->m_alertLabel = label;

  m_widget = topFrame;
  m_widget->setLayout(topLayout);

  this->buildItemFrame();
  this->onPhaseCountChanged();
}

void qtSharedPropertiesItem::buildItemFrame()
{
  // Can easily recreate data because it is the last item in the widget
  // Remove current item frame
  delete this->Internals->m_itemFrame;
  QBoxLayout* topLayout = dynamic_cast<QBoxLayout*>(m_widget->layout());

  auto itemFrame = new ctkCollapsibleButton(this->parentWidget());
  itemFrame->setText("Common Material Properties");
  itemFrame->setContentsFrameShape(QFrame::Box);
  itemFrame->setCollapsedHeight(0);
  itemFrame->setMinimumWidth(400); // cosmetic hack
  topLayout->addWidget(itemFrame, 1);

  auto itemLayout = new QVBoxLayout();
  itemFrame->setLayout(itemLayout);

  for (std::size_t i = 0; i < this->Internals->m_sharedItem->numberOfItemsPerGroup(); ++i)
  {
    auto item = this->Internals->m_sharedItem->item(i);
    qtItem* childItem = nullptr;

    // Check for ItemView
    auto it = this->Internals->m_itemViewMap.find(item->name());
    if (it == this->Internals->m_itemViewMap.end())
    {
      smtk::view::Configuration::Component comp; // create a default style (empty component)
      qtItemInfo info(item, comp, m_widget, m_itemInfo.baseView());
      childItem = m_itemInfo.uiManager()->createItem(info);
    }
    else
    {
      auto info = it->second;
      info.setParentWidget(m_widget);
      info.setItem(item);
      childItem = m_itemInfo.uiManager()->createItem(info);
    }
    if (childItem != nullptr)
    {
      this->Internals->m_qtItemMap[item->name()] = childItem;
      itemLayout->addWidget(childItem->widget());
      QObject::connect(childItem, &qtItem::modified, this, &qtSharedPropertiesItem::onItemModified);
    }
  } // for (i)

  this->Internals->m_itemFrame = itemFrame;
}
