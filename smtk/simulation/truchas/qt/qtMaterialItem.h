//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtMaterialItem - UI components for Truchas material phase/transition sequence
// .SECTION Description
// .SECTION See Also
// qtItem

#ifndef __smtk_extension_qtMaterialItem_h
#define __smtk_extension_qtMaterialItem_h

#include "smtk/simulation/truchas/qt/Exports.h"

#include "smtk/extension/qt/qtAttributeItemInfo.h"
#include "smtk/extension/qt/qtItem.h"

#include <QString>

#include <set>
#include <string>
#include <vector>

class QAbstractButton;
class qtMaterialItemInternals;
class qtMaterialAttribute;

typedef smtk::extension::qtAttributeItemInfo qtItemInfo;

// Implements custom UI for Truchas materials, which can be a sequence
// of phases with transitions between them. This class is a subclass of
// qtItem, even though it displays two interleaved group items ("phases"
// and "transitions").

class SMTKTRUCHASQTEXT_EXPORT qtMaterialItem : public smtk::extension::qtItem
{
  Q_OBJECT

public:
  qtMaterialItem(const qtItemInfo& phasesInfo, const qtItemInfo& transitionsInfo,
    qtMaterialAttribute* attribute);
  virtual ~qtMaterialItem();

signals:
  void addPhase();
  void removePhase(int);

public slots:
  void updateItemData() override;

protected slots:
  void onElementCollapsed(bool state);
  void onItemModified();
  void onPhaseSelected(QAbstractButton* button);
  void checkStatus(int row);

protected:
  void createWidget() override;

  // Apply user data to phase items that are shadowing a shared property
  void initSharedState();
  QWidget* createElementWidget(
    smtk::attribute::GroupItemPtr groupItem, std::size_t element, const QString& labelString);
  void updateTableItemSizes();
  void setTableRowFromPhase(int row, std::size_t element);
  void setTableRowFromTransition(int row, std::size_t element);
  void addTransitionToTable(int row, int element);
  void setTableRowWidgets(int row, QWidget* widget);
  void swapPhase(std::size_t phaseA, std::size_t phaseB);
  void setTransitionToDefault(int row);
  void setTransitionsToDefault(const std::vector<int>& rows);

  QWidget* createDataWidget();
  void recreateDataWidget();

  void movePhaseDown(int row);
  void movePhaseUp(int row);

  void removeSelected();

  void setRowCollapsed(int row, bool collapsed);

private:
  qtMaterialItemInternals* Internals;

}; // class

#endif
