<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="Test">
      <ItemDefinitions>
        <Group Name="group2" Label="Group2" Extensible="true" NumberOfRequiredValues="1">
          <ItemDefinitions>
            <Double Name="adventure" Label="Custom ItemViews">
              <DefaultValue>42.0</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Instanced" Title="Item View Test" TopLevel="true" FilterByAdvanceLevel="false" FilterByCategory="false">
      <InstancedAttributes>
        <Att Name="Test" Type="Test">
          <ItemViews>
            <View Path="/group2/adventure" Type="TruchasPhaseProperty"></View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>